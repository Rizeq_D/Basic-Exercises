# Basic-Exercises

# Git Learning Project

This repository serves as a practice ground for learning Git. The project consists of a simple text file where you have been adding,
 modifying, and deleting objects to understand various Git commands and version control concepts.

## Introduction

This project was created to enhance understanding of Git version control system. 
The main focus has been on manipulating a simple text file using various Git commands.

## Project Structure

- **text_file.txt**: The main text file that has been the subject of changes.

## Git Commands Used

Throughout the learning process, various Git commands have been used, including but not limited to:

- `git init`: Initialize a new Git repository.
- `git add`: Add changes to the staging area.
- `git commit`: Commit changes to the repository.
- `git log`: View the commit history.
- `git status`: Check the status of changes.
- `git diff`: View the differences between versions.
- `git branch`: Create and manage branches.
- `git merge`: Merge branches.
- `git reset`: Reset the repository to a previous state.

